package com.chen.leet_code.answer;

/**
 * https://leetcode.cn/problems/remove-duplicates-from-sorted-list-ii/?envType=study-plan-v2&envId=top-interview-150
 */
public class Solution82 {
    /**
     * 删除排序链表中的重复元素
     * //这个没有完成
     *
     * @param head
     * @return
     */
    public ListNode deleteDuplicates(ListNode head) {
        ListNode realHeader = null; //需要确定的初始header
        ListNode replace=head; //进行截断的上一个结点
        ListNode first = head;//进行截断的最后结点
        ListNode beforeFirst = head; //进行截断的初始结点
        if (head.next == null) {
            return head;
        }

        while(first.next !=null){
            if(first.val ==first.next.val){
                beforeFirst.next=first.next;
                first=first.next;
            }else{
                if(first == beforeFirst){
                    //证明没有移动
                    if(realHeader ==null){
                        realHeader = first;
                    }
                    replace= first;
                    first=first.next;
                    beforeFirst=first;


                }else{
                   //证明移动了
                    if(realHeader==null){
                        realHeader= first.next;
                    }
                    first=first.next;
                    beforeFirst=first;
                }

            }

        }
        return realHeader;

       /* while (first.next!=null){
            if(first.val==first.next.val){
                first=first.next;
            }else{
                if(beforeFirst.val ==first.val){
                    if(header.val == first.val){
                        header= first.next;
                    }
                }else{

                }
            }
        }*/



    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
