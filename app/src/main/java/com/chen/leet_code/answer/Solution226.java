package com.chen.leet_code.answer;

/**
 *https://leetcode.cn/problems/invert-binary-tree/description/?envType=study-plan-v2&envId=top-interview-150
 *
 *
 *
 * */
public class Solution226 {




    public TreeNode invertTree(TreeNode root ){
        TreeNode temp =null;
        if(root !=null){
            temp = root.left;
            root.left =root.right;
            root.right=temp;
            invertTree(root.left);
            invertTree(root.right);
        }
        return root;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
