package com.chen.leet_code.answer;

/**
 * https://leetcode.cn/problems/maximum-depth-of-binary-tree/description/?envType=study-plan-v2&envId=top-interview-150
 */
public class Solution104 {

    public int maxDepth(TreeNode root) {
        int currentDepth =0;

        if(root !=null){
            currentDepth++;
            int calculateNodeDepth =  calculate(root.left);
            int dep = calculate(root.right);
            if(calculateNodeDepth>=dep){
              currentDepth+=calculateNodeDepth;
            }else{
               currentDepth+=dep;
            }
        }
        return currentDepth;

    }


    public int calculate(TreeNode root){
        int size =0;
        if(root ==null){
            return  size;
        }else{
            size++;
           int left = calculate(root.left);
           int right = calculate(root.right);
           int max = left >= right? left:right;
           size+=max;
        }
        return  size;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
