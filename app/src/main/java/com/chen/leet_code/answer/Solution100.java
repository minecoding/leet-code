package com.chen.leet_code.answer;

/**
 * https://leetcode.cn/problems/same-tree/submissions/529844782/?envType=study-plan-v2&envId=top-interview-150
 *
 *
 *
 * */
public class Solution100 {




    public boolean isSameTree(TreeNode p,TreeNode q ){
        if(p!=null && q !=null){
            if(p.val == q.val){
                return  isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
            }
        }else{
            if(p == q){
                return  true;
            }
        }
        return false;
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
